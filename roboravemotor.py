import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
pins=[17,13,12,18]
rightforward=GPIO.PWM(17,100)
rightbackward=GPIO.PWM(18,100)
lefttbackward=GPIO.PWM(13,100)
leftforward=GPIO.PWM(12,100)
GPIO.setup(pins,GPIO.OUT)
speed=50
def rightturn():
    leftforward.start(speed)
    leftbackward.stop()
    rightforward.stop()
    rightbackward.stop()

def leftturn():
    leftforward.stop()
    leftbackward.stop()
    rightforward.start(speed)
    rightbackward.stop()

def forward():
    leftforward.start(speed)
    leftbackward.stop()
    rightforward.start(speed)
    rightbackward.stop()

    
def backwards():
    leftforward.stop()
    leftbackward.start(speed)
    rightforward.stop()
    rightbackward.start(speed)

rightturn()
time.sleep(2.5)
